
import SwiftUI

struct BarChartView: View {
    var body: some View {
        VStack (spacing: 0){
            VStack{
                HeaderView(titel: "Woche",subtitle: "Übersicht")
                HStack (alignment: .bottom){
                    CapsuleView(namber: 50, day: "M")
                    CapsuleView(namber: 80, day: "D")
                    CapsuleView(namber: 100, day: "M")
                    CapsuleView(namber: 50, day: "D")
                    CapsuleView(namber: 40, day: "F")
                    CapsuleView(namber: 70, day: "S")
                    CapsuleView(namber: 100, day: "S")
                }
                
            }
        }
    }
}

struct ShowsView_Previews: PreviewProvider {
    static var previews: some View {
        BarChartView()
    }
}


