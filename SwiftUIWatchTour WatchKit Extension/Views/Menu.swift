//
//  Menu.swift
//  SwiftUIWatchTour WatchKit Extension
//
//  Created by TheDevBen on 05.02.21.
//

import SwiftUI

struct Menu: View {
    @State private var isPresented = false
    var body: some View {
        VStack {
            Text("Welcome, Food?")
            Image("Rabitt-Happy")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 80)
            Spacer()
            Button("Start"){
                self.isPresented.toggle()
            }
            .foregroundColor(Color.white)
            .fullScreenCover(isPresented: $isPresented, content: RingView.init)
            .background(Color.init("MyColor"))
            .cornerRadius(19)
           
            
        }
    }
}

struct Menu_Previews: PreviewProvider {
    static var previews: some View {
        Menu()
    }
}
