//
//  HeaderView.swift
//  WaterTraker
//
//  Created by Craig Clayton on 11/15/19.
//  Copyright © 2019 Cocoa Academy. All rights reserved.
//

import SwiftUI
// This view is going to be use in different views, that mean this is a child view
// Header of the stack
// we can also use the header to be a child view, because we are going to use it in other place
// next step, how we can make it dynamic that the titel change depends about the chart we are using

struct HeaderView: View {
    let titel: String
    let subtitle: String
    
    
    var body: some View {
        HStack(spacing: 0) {
            Text(titel)
                .fontWeight(.heavy)
            Text(subtitle)
                .fontWeight(.thin)
        }
        .foregroundColor(Color.init("MyColor"))
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(titel: "titel", subtitle: "subtitle")
    }
}
