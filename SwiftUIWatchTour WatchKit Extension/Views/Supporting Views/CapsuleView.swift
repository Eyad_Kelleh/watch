
import SwiftUI
// // The next step
// Create capsule
// How we can create instance of the capsule?
// To keep our code clean, we have to create a child view
// we want to make it dynamic that the day can be change easliy, we can solve this, we make some global variable
struct CapsuleView: View {
    let namber: Int
    let day: String
    
    var body: some View {
        HStack(alignment: .bottom, spacing: 2) {
            VStack{
                VStack{
                    Text("\(namber)")
                        .font(.system(size: 11))
                        .foregroundColor(Color.init("MyColor"))
                    Capsule()
                        .frame(width: 10, height: CGFloat(namber))
                        .foregroundColor(Color.init("MyColor"))
                }

                Text(day)
                    .font(.system(size: 12))
                    .fontWeight(.black)
                    .padding(.top,0)
            }

        }
    }
}

struct CapsuleView_Previews: PreviewProvider {
    static var previews: some View {
        CapsuleView(namber: 0, day: "D")
    }
}
