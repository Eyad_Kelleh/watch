import SwiftUI

struct RingView: View {
    // Array of image names to show
    let images  = ["Rabitt-Happy", "Rabitt-Sad", "Rabitt-Eating"]
    @State var activeImageIndex = 0 // Index of the currently displayed image
    @State var activeRingIndex: Double = 0
//    by every you can change how many second, do you need to do it
    let imageSwitchTimer = Timer.publish(every: 120, on: .main, in: .common)
                                .autoconnect()
    let RingSwitchTimer = Timer.publish(every: 60, on: .main, in: .common)
                                .autoconnect()
    var body: some View {
        VStack{
            HeaderView(titel: "Rabitt", subtitle: "Essen")
            ZStack { // Step 1
                Circle() // Step 2
                    .stroke(lineWidth: 20)
                    .fill(Color(.darkGray))
                Image(images[activeImageIndex])
                    .resizable()
                    .scaledToFit()
                    .rotationEffect(.degrees(-90))
                    .frame(width: 60, height: 100)
                    .onReceive(imageSwitchTimer) { _ in
//                         Go to the next image. If this is the last image, go
//                         back to the image #0
                        self.activeImageIndex = (self.activeImageIndex + 1) % self.images.count
                    }
//
//                Image("Rabitt-Sad")
//                    .resizable()
//                    .scaledToFit()
//                    .rotationEffect(.degrees(-90))
//                    .frame(width: 60, height: 100)
//                Image("Rabitt-Happy-Black")
//                    .resizable()
//                    .scaledToFit()
//                    .rotationEffect(.degrees(-90))
//                    .frame(width: 150, height: 108)
                Circle() // Step
                    .trim(from: CGFloat(self.activeRingIndex), to: 1)
//                    .trim(from: 0.3, to: 1)
                    .stroke(Color.init("MyColor"), style: StrokeStyle(lineWidth: 12,            lineCap: .round, lineJoin: .round))
                    .rotationEffect(.degrees(180))
                    .rotation3DEffect(.degrees(180), axis: (x: 1, y: 0, z: 0))
                    .onReceive(RingSwitchTimer) { _ in
                        self.activeRingIndex = (self.activeRingIndex + 0.1)
                    }
                
            }
            .frame(width: 130, height: 130) // Step 4
            .rotationEffect(.degrees(90), anchor: .center)
            .padding(.top, 10)
    }
}
}

struct RingView_Previews: PreviewProvider {
    static var previews: some View {
        RingView()
    }
}
