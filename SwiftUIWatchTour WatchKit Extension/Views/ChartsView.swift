

import SwiftUI
// Create TabView: with the key TabView
// With .tabViewStyle() modifier to PageTabViewStyle(indexDisplayMode: .automatic), which sets TabView to a page control.
struct ChartsView: View {
    var body: some View {
        TabView {
            Menu()
            WedgeChartView()
            BarChartView()
        }.tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
    }
}

struct ChartsView_Previews: PreviewProvider {
    static var previews: some View {
        ChartsView()
    }
}
