

import SwiftUI

@main
struct SwiftUIWatchTourApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ChartsView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
