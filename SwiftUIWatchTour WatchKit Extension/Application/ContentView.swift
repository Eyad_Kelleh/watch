
import SwiftUI

struct ContentView: View {
    @State private var isPresented = false
    // Creating a static List
    // One for the model 'Charts'
    // Second for Standard push to detail view
    // Create a button with the key "Button"
    var body: some View {
        List {
            Button("Charts"){
                self.isPresented.toggle()
            }
            .fullScreenCover(isPresented: $isPresented, content: ChartsView.init)
            NavigationLink(destination: ColorsView()) { // Step 1
                Text("Colors") // Step 2
            }.navigationTitle("Home") // Step 3
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
